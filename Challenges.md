# Challenges
This is where you can find extra challenges I have set that should push the limits of what you know.  
All of these use Java and no external requirements.

## Part 1
Part 1 is all about manipulating 2D arrays with an Image class.

So the challenge this time is to be able to display this in a way that makes sense.

```java
int[][] a_photo_is_just_an_excel_sheet = {
    {119,25,67,41,61,5,105,57,72,36,23,0,83,4,86,80,67,16,117,63,86,122,63,18,80},
    {112,231,152,203,25,192,138,141,32,154,91,152,116,165,172,188,36,208,200,235,78,246,140,153,67},
    {108,201,0,29,64,167,21,59,111,212,78,190,112,60,126,229,36,109,67,183,27,29,90,226,21},
    {89,200,44,91,45,183,228,205,47,128,186,212,57,242,245,185,26,191,155,186,31,132,145,240,57},
    {84,252,65,47,11,30,111,181,5,38,9,252,95,249,93,53,4,184,45,75,49,208,17,5,113},
    {63,185,141,190,52,197,225,147,39,14,97,208,119,222,242,247,44,160,181,228,113,244,175,244,104},
    {85,91,101,125,85,113,2,117,66,92,13,15,90,45,25,3,51,27,41,121,11,102,80,7,25},
};
```

Hint: The name of the class

## Part 2
The main focus of Part 2 is ArrayList's as well as a focus on using both ``get`` and ``set`` methods.  
Another goal is to show you how different data structures can interact.

The challenge this time stems from one line of the specification:
> We can’t actually play the tracks so for testing purposes displaying the track details on the screen will be considered equivalent to playing the track.

So please create something like this:

![](Media/Challenge2.gif "Part 2 Example")


Hint:  We have already learnt about \n, there are other escape characters that are incredibly useful here.  
Hint2: Windows does not like changing anything that is not the current line of the output.  
Hint3: Perfection is not required, only that it *looks* right.

## Part 3
This time round I am providing two levels of challenges, A and B.  
A is for anyone who can do the assignment like the example I create but would like to go a tad further/better while still keeping within teh bounds of Dermots spec.  
B is for anyone who wants an extra challenge that goes beyond the original spec, this is what the previous challenges were like.

Both A and B relate to Dictonary.java

### A

* There are several regions of duplicated code, try to use one method for it.
* Dermot recommends using [Collections.binarySearch].
  * Use it to see if the ArrayList contains the word.
  * Use it to insert a new word while keeping the order.

[Collections.binarySearch]: https://www.google.com/search?q=collections+binarysearch+java

### B

The core of this assignment is the ability to read and parse files.  
The challenge this time is to reverse that.  

Each step will be slightly harder than the last.  

* Output the contents of the ArrayList to a file.
* With lowercase words
* Formatted as a .csv
* With the same root path as the original.
* With a filename of ``{original}_modified_0``
  * With each line being max 80 characters long.
* With a filename of ``{original}_modified_1``
  * Each line only has words that start with the same letter.
  * Header row is ``Word 1,Word 2,Word 3, ...``
  * each line should have teh same number of commas


### B Output examples

Output:  
![](Media/Challenge3.PNG "Part 3 Example")


``{original}_modified_0.csv``

```csv
after,amusement,apple,art,attack,bite,brass,bread,but,card,chain,cold,connection
credit,curtain,destruction,disease,drain,drawer,edge,elastic,electric,father
field,finger,fire,fixed,hair,hanging,happy,hole,hope,hospital,increase,interest
iron,jump,knee,knot,mark,mine,name,nation,opposite,plough,rail,ray,reason,sea
shame,soap,soft,steel,stomach,summer,sun,support,take,thin,town,train,tree
trouble,twist,unit,violent,waiting,waste,worm,writing
```

``{original}_modified_1.csv``
```csv
Word 1,Word 2,Word 3,Word 4,Word 5,Word 6,Word 7,Word 8,Word 9
after,amusement,apple,art,attack,,,,
bite,brass,bread,but,,,,,
card,chain,cold,connection,credit,curtain,,,
destruction,disease,drain,drawer,,,,,
edge,elastic,electric,,,,,,
father,field,finger,fire,fixed,,,,
hair,hanging,happy,hole,hope,hospital,,,
increase,interest,iron,,,,,,
jump,,,,,,,,
knee,knot,,,,,,,
mark,mine,,,,,,,
name,nation,,,,,,,
opposite,,,,,,,,
plough,,,,,,,,
rail,ray,reason,,,,,,
sea,shame,soap,soft,steel,stomach,summer,sun,support
take,thin,town,train,tree,trouble,twist,,
unit,,,,,,,,
violent,,,,,,,,
waiting,waste,worm,writing,,,,,
```

``{original}_modified_1`` is a csv format that is most widespread, where every cell is accounted for.  
Most programs will accept ``{original}`` and ``{original}_modified_0`` but older programs/scripts may assume its properly formatted.  
This is in part befcause there is no offical spec for csv, only what is most used.  
And that is before we go into any characters that may break the file, like an address normally has commas in it.   

## Part 4
Part of the goal for this assignment is to emulate code that is already in production.  
We are given a file with two methods complete and are required to add more functions.

### A
Mot too much to add here this time, it's relatively straight forward.

* Where possible try not to change ``main`` or ``calculateApplicantScores``

### B
This time round do not submit this challenge as a solution.  
I honestly couldnt think about one that could fit into teh submission.

So create a ``ApplicantAnalysisV2.java`` and rewrite the class as you please.

* The Minister for Educationist has decreed that Honors Maths will get 25% more points if its in teh final 6 grades.
  * It will get 0 bonus marks if the base value is not in teh top 6.
* The maths grade will always be in teh second last result on the input line.
  * Highlighted in the example below  
    21858000,O8,H6,H1,H8,**O7**,H2,,,  
    21103598,H1,H8,O5,H3,H7,H6,H7,**H8**,H6  
* For bonus challenge try to only apply the bonus for honors maths.