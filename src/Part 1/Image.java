/*
ID:     12136891
Name:   Brendan Golden
Module: CS4222
Date:   2022-02-27
*/

import java.util.Arrays;

public class Image {
    private int[][] pixels;
    private int width;
    private int height;

    public Image(int[][] pixels) {
        this. pixels = pixels;
        this.height = pixels.length;
        this.width = pixels[0].length;
    }

    public String toString(){
        return Arrays.deepToString(pixels);
    }

    public void flip( boolean horizontal ){
        if(horizontal){
            for(int row=0; row < height; row++){
                for(int col=0; col < width/2; col++){
                    // figure out its mirrored counterpart
                    int col_mirror = width - 1 - col;

                    // swap things around
                    int temp = pixels[row][col];
                    pixels[row][col] = pixels[row][col_mirror];
                    pixels[row][col_mirror] = temp;
                }
            }
        } else {
            // because we are swapping rows we dont need a nestled loop,
            // but we do need a different temp value

            for(int row=0; row < height/2; row++){
                int row_mirror = height - 1 - row;

                int[] temp = pixels[row];
                pixels[row] = pixels[row_mirror];
                pixels[row_mirror] = temp;
            }
        }
    }


    public void rotate( boolean clockwise ){
        int[][] pixels_new = new int[width][height];
        if(clockwise){
            for(int row=0; row < height; row++){
                for(int col=0; col < width; col++){
                    pixels_new[col][height-1-row] = pixels[row][col];
                }
            }
        } else {
            for(int row=0; row < height; row++){
                for(int col=0; col < width; col++){
                    pixels_new[width-1-col][row] = pixels[row][col];
                }
            }
        }

        this.pixels = pixels_new;
        int h_new = width;
        int w_new = height;
        this.height = h_new;
        this.width = w_new;
    }
}