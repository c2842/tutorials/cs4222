/*
ID:     12136891
Name:   Brendan Golden
Module: CS4222
Date:   2022-02-27
*/
/*
Date format is ISO 8601
https://en.wikipedia.org/wiki/ISO_8601
*/


public class ImageDriver {
    public static void main(String[] args) {
        // reference: https://studio.youtube.com/watch?v=UBX2QQHlQ_I
        int[][] a_photo_is_just_an_excel_sheet = {
                {1, 2, 3},
                {4, 5, 6},
        };

        Image picture = new Image(a_photo_is_just_an_excel_sheet);
        System.out.println(picture.toString());
        System.out.println(picture); // Automatically calls the toString method

        picture.rotate(true);
        picture.rotate(true);
        picture.rotate(true);
        System.out.println(picture);

        picture.rotate(false);
        System.out.println(picture);

        picture.flip(false);
        System.out.println(picture);

        picture.flip(true);
        System.out.println(picture);
    }
}
