import java.util.ArrayList;
import java.util.Arrays;

public class PlayList {

    String name;
    ArrayList<Track> tracks;

    // Create a PlayList with a default name (e.g. My PlayList)
    public PlayList(){
        this.name = "My PlayList";
        this.tracks = new ArrayList<>();
    }
    // Create a PlayList with the specified name
    public PlayList(String playListName){
        this.name = playListName;
        this.tracks = new ArrayList<>();
    }

    public String toString(){
        ArrayList<String> result = new ArrayList<>();

        for (Track track: this.tracks){
            result.add(track.toString());
        }

        return result.toString();
    }

    // Allows name change
    public void setName(String name){
        this.name = name;
    }

    // Returns current name
    public String getName(){
        return this.name;
    }


    // Add a Track where only the title and artist are known
    // The year and duration should be set to zero
    public void add(String title, String artist){
        this.tracks.add(new Track(title, artist));
    }

    // Add a Track where ALL of the data is known
    public void add(String title, String artist, int year, int duration){
        this.tracks.add(new Track(title, artist,year, duration));
    }

    // Add a previously created instance of the Track class
    public void add(Track t){
        this.tracks.add(t);
    }


    /*
    The method is passed the title of the track to be removed.
    If there is a track in the playlist with the same title the track should be removed and the method returns true.
    You should use a case-insensitive comparison when matching track titles.
    If there is no track in the playlist with the specified title the method should return false and the list should be left unchanged.
     */
    public boolean remove(String title){
        boolean result = false;

        for(int i=this.tracks.size()-1; i>= 0; i--){
            Track track = this.tracks.get(i);

            if(track.getTitle().equalsIgnoreCase(title)){
                tracks.remove(i);
                result = true;
            }
        }

        return result;
    }


    public void showList(){
        if(this.tracks.size() == 0){
            System.out.println("The list is empty");

            // return early
            // return;
        }

        for(Track track: this.tracks){
            System.out.println(track);
        }
    }

    /**
     You may find it useful to develop additional “helper” methods that might simplify
     or improve the efficiency of some operations, or might reduce the amount of code you have to write.
     ALL helper methods should be private.

     ************************************************************************************************
     In addition, playing the tracks randomly should NOT alter the sequence of the tracks in the list.
     ************************************************************************************************

     */

    public void playAll(boolean random){
        int size = this.tracks.size();

        int[] playOrder = new int[size];

        if(!random){
            for(int i=0;i < size;i++){
                playOrder[i] = i;
            }
        } else{
            // minesweeper code?

            // quick of new array is that all values are 0
            int[] tmp = new int[size];

            int filled = 0;
            while (filled < size){
                int position = (int)(Math.random() * size);

                if(tmp[position] == 0){
                    // save the position in the order array
                    playOrder[filled] = position;

                    // mark the position as used
                    tmp[position] = 1;

                    // move onto the next spot in teh playOrder
                    filled += 1;
                }
            }
        }

        playTracks(playOrder);
    }

    public void playOnly(String artist){
        int size = this.tracks.size();
        int[] playOrder = new int[size];

        int filled = 0;
        for(int i=0;i < size;i++){
            Track track = this.tracks.get(i);

            if(track.getArtist().toLowerCase().contains(artist.toLowerCase())){
                playOrder[filled] = i;
                filled += 1;
            }
        }

        if(filled < size){
            // resize it to fit
            playOrder = Arrays.copyOf(playOrder, filled);
        }

        playTracks(playOrder);
    }
    public void playOnly(int year){
        int size = this.tracks.size();
        int[] playOrder = new int[size];

        int filled = 0;
        for(int i=0;i < size;i++){
            Track track = this.tracks.get(i);

            if(track.getYear() == year){
                playOrder[filled] = i;
                filled += 1;
            }
        }

        if(filled < size){
            // resize it to fit
            playOrder = Arrays.copyOf(playOrder, filled);
        }

        playTracks(playOrder);
    }

    private void playTracks (int[] playOrder){
        for(int index: playOrder){
            System.out.println(this.tracks.get(index));
        }
    }
}
