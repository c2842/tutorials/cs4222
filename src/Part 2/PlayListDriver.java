public class PlayListDriver {

    /*
    The Driver class will be used to test the other classes and
    will contain just a main method that uses ALL of the methods provided in the other two classes to show that they have been tested
     */

    public static void main(String[] args) {
        // Create a new playlist
        PlayList list = new PlayList();

        // check the name
        System.out.println(list.getName());

        // I dont like "my Playlist", lets create  new playlist with a better name
        list = new PlayList("CS4141");

        // oh feck wrong module
        list.setName("CS4222");


        // adding our favourite tracks
        list.add("Sound of Silence", "Life");
        list.add("Sound of Silence", "Disturbed");
        list.add("Sound of Silence", "Simon & Garfunkel");

        list.showList();

        // feck, all of them have 0 duration
        if(list.remove("Sound of Silence")){
            System.out.println("Successfully removed the bad data");
        }

        // properly add them this time
        list.add("Sound of Silence", "Life", 2020, 64084342); // it's still March 2020 right?
        list.add("Sound of Silence", "Disturbed", 2015, (4 * 60) + 19 ); // 4:19
        list.add("Sound of Silence", "Simon & Garfunkel", 1981, (3 * 60) + 58); // 3:58

        // now print out the contents (uses toString automatically
        System.out.println(list);
        System.out.println();

        // lets start playing stuff
        list.playAll(false);
        System.out.println();

        list.playAll(true);
        System.out.println();

        // complete match
        list.playOnly("Life");
        System.out.println();

        // partial
        list.playOnly("Simon");
        System.out.println();

        // no match
        list.playOnly("Jedward");
        System.out.println();

        // should have one match
        list.playOnly(2015);
        System.out.println();

        // who listens to modern music
        list.playOnly(2022);
        System.out.println();


        // time to use teh track methods

        // create the best track
        Track track = new Track("Never Gonna Give Ye Up", "Rick R Astley");

        // no no no thats still missing some info
        track = new Track("Never Gonna Give Ye Up", "Rick R Astley", 2009);

        // no no no thats still missing some info ye idiot
        track = new Track("Never Gonna Give Ye Up", "Rick R Astley", 2009, 215);

        // lets see it in proper detail
        System.out.println(track);

        // oh no we typoed it, time to fix
        if(track.getTitle().equals("Never Gonna Give Ye Up")){
            track.setTitle("Never Gonna Give You Up");
        }
        if(track.getArtist().equals("Rick R Astley")){
            track.setArtist("Rick Astley");
        }
        if(track.getYear() == 2009){
            track.setYear(1987);
        }
        // finally we want the 10 hour version of brilliance
        if(track.getDuration() == 215){
            track.setDuration(36000);
        }

        // just to be sure print out the details
        System.out.println(track);

        // finally add it to the playlist
        list.add(track);

        // play out the ending
        list.playOnly(1987);
    }
}
