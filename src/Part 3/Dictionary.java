import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Dictionary {
    private final ArrayList<String> words ;
    private final int shortest;
    private final int longest;

    public Dictionary(String filepath, int shortest, int longest) {
        // This method reads words from the specified file
        // and adds them to the ‘words’ list. See details below.

        this.words = new ArrayList<>();
        this.shortest = shortest;
        this.longest = longest;

        try{
            File dataFile = new File(filepath);
            Scanner dataSource = new Scanner(dataFile);

            while(dataSource.hasNextLine()) {
                String line = dataSource.nextLine();
                String[] line_split = line.split(",");

                for(String word: line_split){
                    String word_trimmed = word.trim();

                    String word_uppercase = word_trimmed.toUpperCase();

                    if(word_trimmed.length() >= this.shortest && word_trimmed.length() <= this.longest){
                        if(!this.words.contains(word_uppercase)){
                            this.words.add(word_uppercase);
                        }
                    }
                }
            }

            Collections.sort(this.words);

        }catch (IOException e){
            System.out.println("File does not exist");
        }
    }

    // You will also need to include the other methods specified below.
    public boolean add(String word){
        String word_trimmed = word.trim();
        String word_uppercase = word_trimmed.toUpperCase();

        if(word_trimmed.length() >= this.shortest && word_trimmed.length() <= this.longest){
            if(!this.words.contains(word_uppercase)){
                this.words.add(word_uppercase);
                Collections.sort(this.words);
                return true;
            }
        }

        return false;
    }

    public String nextWord(){
        if(this.words.size() == 0){
            return "";
        }
        int random = (int)(Math.random() * this.words.size());
        return this.words.get(random);
    }

    public boolean inDictionary(String word){
        String word_trimmed = word.trim();
        String word_uppercase = word_trimmed.toUpperCase();

        return this.words.contains(word_uppercase);
    }
}
